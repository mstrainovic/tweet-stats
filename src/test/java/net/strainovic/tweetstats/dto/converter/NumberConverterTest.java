package net.strainovic.tweetstats.dto.converter;

import org.junit.Assert;
import org.junit.Test;

public class NumberConverterTest {

    @Test
    public void numberConversionTest() {
	// Over 1M
	String converted = NumberConverter.convertNumberToString(1000000);

	Assert.assertEquals("1M", converted);

	converted = NumberConverter.convertNumberToString(1111111);

	Assert.assertEquals("1.1M", converted);

	converted = NumberConverter.convertNumberToString(10000001);

	Assert.assertEquals("10M", converted);

	// Over 1K
	converted = NumberConverter.convertNumberToString(1000);

	Assert.assertEquals("1k", converted);

	converted = NumberConverter.convertNumberToString(11000);

	Assert.assertEquals("11k", converted);

	converted = NumberConverter.convertNumberToString(15150);

	Assert.assertEquals("15k", converted);
    }
}
