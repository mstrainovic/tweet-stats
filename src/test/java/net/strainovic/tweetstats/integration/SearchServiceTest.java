package net.strainovic.tweetstats.integration;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import net.strainovic.tweetstats.dto.DashboardDataDto;
import net.strainovic.tweetstats.service.SearchService;

public class SearchServiceTest {

    @Autowired
    private SearchService searchService;

    private SearchService getSearchService() {
	return searchService;
    }

    //TODO this need to be implemented
    @Ignore
    @Test
    public void multipleUsers() {
	DashboardDataDto dashboardData = getSearchService().getDashboardData("SundayMorning");

	dashboardData.getTopFollowers().forEach(userDto -> System.out.println(userDto.getScreenName()));
    }

}
