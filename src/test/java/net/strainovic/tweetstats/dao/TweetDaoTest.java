package net.strainovic.tweetstats.dao;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import net.strainovic.tweetstats.model.Tweet;
import net.strainovic.tweetstats.model.TweetUser;

@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class TweetDaoTest {

    @Autowired
    private TweetDao tweetDao;

    @Test
    public void saveMultipleTweetWithOneUser() {
	TweetUser tweetUser1 = new TweetUser();
	tweetUser1.setId(1);
	tweetUser1.setFollowersCount(10);

	Tweet tweet1 = new Tweet();
	tweet1.setId(1L);
	tweet1.setText("Some tweet text 1");
	tweet1.setTweetUser(tweetUser1);

	getTweetDao().save(tweet1);

	Tweet tweetOne = getTweetDao().findOne(1L);
	Assert.assertEquals("Some tweet text 1", tweetOne.getText());
	Assert.assertEquals(1L, tweetOne.getTweetUser().getId());
	Assert.assertEquals(10, tweetOne.getTweetUser().getFollowersCount());

	// Second tweet
	TweetUser tweetUser2 = new TweetUser();
	tweetUser2.setId(1);
	tweetUser2.setFollowersCount(11);

	Tweet tweet2 = new Tweet();
	tweet2.setId(2L);
	tweet2.setText("Some tweet text 2");
	tweet2.setTweetUser(tweetUser2);

	getTweetDao().save(tweet2);

	Tweet tweetTwo = getTweetDao().findOne(2L);
	Assert.assertEquals("Some tweet text 2", tweetTwo.getText());
	Assert.assertEquals(1L, tweetTwo.getTweetUser().getId());
	Assert.assertEquals(11, tweetTwo.getTweetUser().getFollowersCount());

	Assert.assertEquals(tweetOne.getTweetUser().getId(), tweetTwo.getTweetUser().getId());
    }

    @Test
    public void saveOneTweetWithExistingId() {
	Tweet tweet1 = new Tweet();
	tweet1.setId(1L);
	tweet1.setText("Some tweet text 1");

	getTweetDao().save(tweet1);

	Tweet tweetOne = getTweetDao().findOne(1L);
	Assert.assertEquals("Some tweet text 1", tweetOne.getText());
    }

    // TODO add this to service tests
    @Test
    public void hashTagRatioTest() {
	Tweet tweet1 = new Tweet();
	tweet1.setId(1L);
	tweet1.setText("Some tweet text 1");
	tweet1.setHashtagEntities(Collections.singletonList("manutd"));

	Tweet tweet2 = new Tweet();
	tweet2.setId(2L);
	tweet2.setText("Some tweet text 2");
	tweet2.setHashtagEntities(Arrays.asList("manutd", "liverpool"));

	Tweet tweet3 = new Tweet();
	tweet3.setId(3L);
	tweet3.setText("Some tweet text 3");
	tweet3.setHashtagEntities(Arrays.asList("manutd", "liverpool", "chelsea"));

	List<Tweet> tweets = Arrays.asList(tweet1, tweet2, tweet3);

	Map<String, Long> collect = tweets.stream().flatMap(t -> t.getHashtagEntities().stream())
		.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

	Assert.assertEquals(Long.valueOf(3L), collect.get("manutd"));
	Assert.assertEquals(Long.valueOf(2L), collect.get("liverpool"));
	Assert.assertEquals(Long.valueOf(1L), collect.get("chelsea"));

    }

    private TweetDao getTweetDao() {
	return tweetDao;
    }

}
