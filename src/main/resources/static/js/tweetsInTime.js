$.ajax({

    url: window.location.href + "/" + "tweetsInTime",
    data: [],
    success: function(numbers){

        Morris.Area({
            element: 'morris-area-chart',
            data: numbers,
            xkey: 'key',
            ykeys: ['count'],
            labels: ['Tweets in time'],
            pointSize: 0,
            fillOpacity: 0.9,
            pointStrokeColors:['#16198d'],
            behaveLikeLine: false,
            gridLineColor: '#eef0f2',
            lineWidth: 0,
            hideHover: 'auto',
            lineColors: ['#16198d'],
            resize: true

        });
    }
});

 $('.vcarousel').carousel({
            interval: 3000
         });