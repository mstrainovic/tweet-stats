<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en"
      xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Tweet Stats</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>

    <!-- jQuery 2.0.2 -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="/js/jquery.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/plugins/chart.js" type="text/javascript"></script>

    <style type="text/css">
    </style>
</head>
<body class="skin-black">
<jsp:include page="partials/header.jsp"/>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <jsp:include page="partials/sidebar.jsp"/>

    <aside class="right-side">
        <section class="content">
            <jsp:include page="partials/user/userNumbers.jsp"/>
            <jsp:include page="partials/user/userData.jsp"/>
        </section>
    </aside>


</div>
</body>