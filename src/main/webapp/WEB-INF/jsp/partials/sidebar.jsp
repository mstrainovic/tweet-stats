<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="${user.picture}" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Hello</p>
                <p>${user.name}</p>
            </div>
        </div>
        <!-- history -->
        <ul class="sidebar-menu" style="margin-bottom:5px;">
            <c:forEach items="${histories}" var="history">
                <c:set var="currentUrl" value="${requestScope['javax.servlet.forward.request_uri']}"/>
                <c:set var="tag" value="${history.hashTag}"/>
                <li class="<c:if test="${fn:endsWith(currentUrl, tag)}"> active</c:if>">
                    <a href="<c:url value="/secured/dashboard/${history.hashTag}" />" class="btn"
                       title="${history.processing ? 'Collecting tweets click for new results' : 'Collecting tweets done'}">
                        <i class="fa"></i><span>#${history.hashTag}</span> <span
                            class="${history.processing ? 'glyphicon glyphicon-refresh glyphicon-refresh-animate' : ''}"></span>
                    </a>
                </li>
            </c:forEach>
        </ul>
        <!-- /.history -->
    </section>
    <!-- /.sidebar -->
</aside>
