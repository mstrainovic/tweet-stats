<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
    <div class="col-md-3">
        <!-- user data -->
        <div class="profile_data">
            <h2 class="name">${userData.name}<span class="verified">${userData.verified}</span></h2>
            <div class="description">
                <p>${userData.description}</p>
            </div>
            <ul class="other">
                <li>${userData.email}</li>
                <li>${userData.createdAt}<br>&nbsp;</li>
                <li>${userData.timeZone}</li>
                <li>${userData.lang}</li>
            </ul>
            <!-- add tweet to and message action -->
        </div>
        <!-- END user data -->
    </div>
    <div class="col-md-9">
        <section class="panel">
            <div class="panel-body">
                <h1>Tweets</h1>

                <h2>${userBoardData}</h2>

                <h3>${userBoardData.nesto}</h3>
                <h3>${userBoardData.user}</h3>
                <h3>${userBoardData.tweets[0].text}</h3>

                <c:forEach items="${tweetsMap}" var="tweetMap">
                    <c:if test="${!empty tweetMap.value}">
                        <h3>${tweetMap.key}</h3>

                        <c:forEach items="${tweetMap.value}" var="tweet">

                            <ul class="media-list">
                                <!-- tweets -->
                                <li class="media">
                                    <div class="media-left">
                                        <a href="<c:url value="user/${tweet.user.screenName}" />"
                                           class="pull-left">
                                            <img src="${tweet.user.profileImageUrl}" alt="Avatar"
                                                 class="img-circle"
                                                 width="64"
                                                 height="64">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <div class="media-heading">
                                <span class="text-muted pull-right">
                                    <small><em>Retweeted count: <strong>${tweet.retweetCount}</strong></em></small>
                                </span>

                                            <a href="<c:url value="user/${tweet.user.screenName}" />"
                                               class="text-danger pull-left">
                                                <strong>${tweet.user.name}</strong>
                                            </a>
                                        </div>
                                        <p>
                                                ${tweet.text}
                                        </p>
                                        <c:forEach items="${tweet.medias}" var="media">
                                            <a href="${media.mediaUrl}">
                                                <img id="${media.id}" src="${media.mediaUrl}" alt=""
                                                     width="200">
                                            </a>
                                        </c:forEach>
                                    </div>
                                </li>
                            </ul>
                        </c:forEach>
                    </c:if>
                </c:forEach>


            </div>
        </section>
    </div>
</div>