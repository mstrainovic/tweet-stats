<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <div class="panel-body">

                <div class="profile_banner">
                <img alt="" src="${userData.profileBannerImageUrl}">
                </div>
                <div class="col-md-3">
                    <div class="profile_avatar_container">
                        <div class="profile_avatar">
                            <img class="profile_avatar-image "
                                 src="${userData.profileImageUrl}"
                                 alt="Apartment Goals">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="sm-st clearfix">
                        <div class="sm-st-info">
                            <span>${userData.followersCount}</span>
                            Followers Count
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="sm-st clearfix">
                        <div class="sm-st-info">
                            <span>${userData.friendsCount}</span>
                            Friends Count
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="sm-st clearfix">
                        <div class="sm-st-info">
                            <span>${userData.statusesCount}</span>
                            Statuses Count
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="sm-st clearfix">
                        <div class="sm-st-info">
                            <span>${userData.favouritesCount}</span>
                            Favourites Count
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>