<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="navbar-default sidebar nicescroll" role="navigation">
    <div class="sidebar-nav navbar-collapse ">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                <!-- /input-group -->
            </li>
            <li class="nav-small-cap">History</li>
            <c:forEach items="${histories}" var="history">
                <c:set var="currentUrl" value="${requestScope['javax.servlet.forward.request_uri']}"/>
                <c:set var="tag" value="${history.hashTag}"/>
                <li class="<c:if test="${fn:endsWith(currentUrl, tag)}"> active</c:if>">
                    <a href="<c:url value="/secured/dashboard/${history.hashTag}" />" class="waves-effect"
                       title="${history.processing ? 'Collecting tweets click for new results' : 'Collecting tweets done'}">
                        <i class="ti-widget fa-fw"></i><span>#${history.hashTag}</span> <span
                            class="${history.processing ? 'glyphicon glyphicon-refresh glyphicon-refresh-animate' : ''}"></span> <!-- samo proveri ovo  -->
                    </a>
                </li>
            </c:forEach>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>