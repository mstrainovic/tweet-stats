<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
    <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
        <div class="top-left-part"><a class="logo" href="/secured/dashboard/"><i class="glyphicon glyphicon-fire"></i>&nbsp;<span class="hidden-xs">Tweet Stats</span></a></div>
        <ul class="nav navbar-top-links navbar-left hidden-xs">
            <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
        </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">
            <li>
                <form role="search" action="/secured/search" method="post" class="app-search hidden-xs">
                    <input type="text" name="hashTag" placeholder="Search..." class="form-control">
                    <a href="#" ><i class="fa fa-search"></i></a>

                </form>
            </li>
            <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="${user.picture}" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">${user.name}</b> </a>
            </li>
        </ul>
    </div>
</nav>
