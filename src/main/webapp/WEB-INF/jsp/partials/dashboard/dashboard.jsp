<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-12">
                <h4 class="page-title">Welcome to Tweet Stats searched tag is #${hashTag}</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <jsp:include page="tweetsNumbers.jsp"/>

        <div class="row">
            <div class="col-sm-6 col-xs-12 col-md-6 col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="bar-widget">
                                <div class="table-box">
                                    <div class="cell text-left">
                                        <div id="sparkline1"></div>
                                    </div>
                                    <div class="cell text-right">
                                        <h2 class="m-t-0 m-b-5 font-light counter">3506</h2>
                                        <h5 class="text-muted m-b-0 m-t-0">Sales</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="bar-widget">
                                <div class="table-box">
                                    <div class="cell text-left">
                                        <div id="sparkline2"></div>
                                    </div>
                                    <div class="cell text-right">
                                        <h2 class="m-t-0 m-b-5 font-light">18% Total</h2>
                                        <h5 class="text-muted m-b-0 m-t-0">Visitors</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Portlet -->
            </div>
            <div class="col-sm-6 col-xs-12 col-md-6 col-lg-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="col-md-5 col-sm-4">
                                <h4 class="font-light">India,<br/>
                                    Ahmedabad</h4>
                                <p><b>1,20,000 <br/>
                                    USD</b></p>
                            </div>
                            <div class="col-md-7 col-sm-8">
                                <div id="sparkline3" class="text-center"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12 col-md-6 col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="bar-widget">
                                <div class="table-box">
                                    <div class="cell text-left">
                                        <div id="sparkline4"></div>
                                    </div>
                                    <div class="cell text-right">
                                        <h2 class="m-t-0 m-b-5 font-light counter">2547</h2>
                                        <h5 class="text-muted m-b-0 m-t-0">clicks</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="bar-widget">
                                <div class="table-box">
                                    <div class="cell text-left">
                                        <div id="sparkline6"></div>
                                    </div>
                                    <div class="cell text-right">
                                        <h2 class="m-t-0 m-b-5 font-light counter">1354</h2>
                                        <h5 class="text-muted m-b-0 m-t-0">Views</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12 col-md-6 col-lg-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="col-md-3 col-sm-3">
                                <h4 class="font-light">Total<br/>
                                    Sales</h4>
                                <p><b>1,20,000 <br/>
                                    USD</b></p>
                            </div>
                            <div class="col-md-9 col-sm-9">
                                <div id="sparkline5"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- /Portlet -->
            </div>
        </div>
        <!--/ row -->
        <jsp:include page="tweetsTable.jsp"/>
        <jsp:include page="usersTable.jsp"/>
    </div>
    <!-- /.container-fluid -->
</div>