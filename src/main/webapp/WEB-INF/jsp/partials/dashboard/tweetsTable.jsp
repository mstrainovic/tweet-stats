<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- row -->
<div class="row">
    <div class="col-md-4 col-xs-12 col-sm-6">
        <div class="white-box">
            <h3>Top 5 retweeted</h3>
            <div class="message-center">
            <c:forEach items="${dashboard.topRetweeted}" var="tweet">
            <a href="<c:url value="user/${tweet.user.screenName}" />">
                <div class="user-img"> <img src="${tweet.user.profileImageUrl}" alt="user" class="img-circle"></div>
                <div class="mail-contnet">
                    <h5>${tweet.user.name}<span class="time pull-right">Retweeted count: ${tweet.retweetCount}</span></h5>
                    <span class="mail-desc">${tweet.text}</span></div>
            </a>
            </c:forEach>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-xs-12 col-sm-6">
        <div class="white-box">
            <h3>Top 5 favorited</h3>
            <div class="message-center">
                <c:forEach items="${dashboard.topFavorited}" var="tweet">
                    <a href="<c:url value="user/${tweet.user.screenName}" />">
                        <div class="user-img"> <img src="${tweet.user.profileImageUrl}" alt="user" class="img-circle"></div>
                        <div class="mail-contnet">
                            <h5>${tweet.user.name}<span class="time pull-right">Favorite count: ${tweet.favoritedCount}</span></h5>
                            <span class="mail-desc">${tweet.text}</span></div>
                    </a>
                </c:forEach>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-xs-12 col-sm-6">
        <div class="white-box">
            <h3>Last 5</h3>
            <div class="message-center">
                <c:forEach items="${dashboard.lastTweets}" var="tweet">
                    <a href="<c:url value="user/${tweet.user.screenName}" />">
                        <div class="user-img"> <img src="${tweet.user.profileImageUrl}" alt="user" class="img-circle"></div>
                        <div class="mail-contnet">
                            <h5>${tweet.user.name}<span class="time pull-right">Created ago: ${tweet.createdAgo}</span></h5>
                            <span class="mail-desc">${tweet.text}</span></div>
                    </a>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
