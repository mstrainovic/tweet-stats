<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- /.row -->
<div class="row">
    <div class="col-md-8 col-lg-12 col-sm-12">
        <div class="white-box">
            <div class="row row-in">
                <div class="col-lg-3 col-sm-6">
                    <div class="col-in text-center">
                        <h5 class="text-danger">Processed Tweets</h5>
                        <h3 class="counter">${dashboard.numbers.totalTweetCount}</h3>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="col-in text-center b-r-none">
                        <h5 class="text-muted text-warning">Unique Users</h5>
                        <h3 class="counter">${dashboard.numbers.uniqueUsers}</h3>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="col-in text-center">
                        <h5 class="text-muted text-purple">Retweet Count</h5>
                        <h3 class="counter">${dashboard.numbers.retweetCount}</h3>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="col-in text-center b-0">
                        <h5 class="text-info">Favorite Count</h5>
                        <h3 class="counter">${dashboard.numbers.favoriteCount}</h3>
                    </div>
                </div>
            </div>
            <div id="morris-area-chart" style="height: 345px;"></div>
        </div>
    </div>
</div>
