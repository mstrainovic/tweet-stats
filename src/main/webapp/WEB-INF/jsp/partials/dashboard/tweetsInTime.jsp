<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- Main row -->
<div class="row">
    <div class="col-md-12">
        <!--earning graph start-->
        <section class="panel">
            <header class="panel-heading">
                Tweets In Time
            </header>
            <div class="panel-body">
                <canvas id="linechart" width="600" height="330"></canvas>
            </div>
        </section>
        <!--earning graph end-->
    </div>
</div>
<script type="text/javascript">
    function char() {
        var times = [];
        <c:forEach items="${dashboard.tweetsInTimeData.times}" var="time">
        times.push("${time}");
        </c:forEach>

        var numbers = [];
        <c:forEach items="${dashboard.tweetsInTimeData.numbers}" var="count">
        numbers.push(${count});
        </c:forEach>


        console.log("times", times);
        console.log("numbers", numbers);

        "use strict";
        //BAR CHART
        var data = {
            labels: times,
            datasets: [
                {
                    label: "My Second dataset",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: numbers
                }
            ]
        };
        new Chart(document.getElementById("linechart").getContext("2d")).Line(data, {
            responsive: true,
            maintainAspectRatio: false
        });

    }
    // Chart.defaults.global.responsive = true;
    $(document).ready(char());
</script>