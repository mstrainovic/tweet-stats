<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- row -->
<div class="row">
    <div class="col-md-4 col-xs-12 col-sm-6">
        <div class="white-box">
            <h3>Top Followers Users</h3>
            <div class="message-center">
                <c:forEach items="${dashboard.topFollowers}" var="tweetUser">
                    <a href="<c:url value="user/${tweetUser.screenName}" />">
                        <div class="user-img"> <img src="${tweetUser.profileImageUrl}" alt="user" class="img-circle"></div>
                        <div class="mail-contnet">
                            <h5>${tweetUser.name}<span class="time pull-right">Followers count: ${tweetUser.followersCount}</span></h5>
                            <span class="mail-desc">${tweetUser.description}</span></div>
                    </a>
                </c:forEach>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-xs-12 col-sm-6">
        <div class="white-box">
            <h3>Top Statuses Users</h3>
            <div class="message-center">
                <c:forEach items="${dashboard.topStatuses}" var="tweetUser">
                    <a href="<c:url value="user/${tweetUser.screenName}" />">
                        <div class="user-img"> <img src="${tweetUser.profileImageUrl}" alt="user" class="img-circle"></div>
                        <div class="mail-contnet">
                            <h5>${tweetUser.name}<span class="time pull-right">Statuses count: ${tweetUser.statusesCount}</span></h5>
                            <span class="mail-desc">${tweetUser.description}</span></div>
                    </a>
                </c:forEach>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-xs-12 col-sm-6">
        <div class="white-box">
            <h3>Top Friends Users</h3>
            <div class="message-center">
                <c:forEach items="${dashboard.topFriends}" var="tweetUser">
                    <a href="<c:url value="user/${tweetUser.screenName}" />">
                        <div class="user-img"> <img src="${tweetUser.profileImageUrl}" alt="user" class="img-circle"></div>
                        <div class="mail-contnet">
                            <h5>${tweetUser.name}<span class="time pull-right">Friends count: ${tweetUser.friendsCount}</span></h5>
                            <span class="mail-desc">${tweetUser.description}</span></div>
                    </a>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
