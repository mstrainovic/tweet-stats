<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- Main row -->
<div class="row">
    <div class="col-md-12">
        <!--earning graph start-->
        <section class="panel">
            <header class="panel-heading">
                Tweets In Time
            </header>
            <div class="panel-body">
                <canvas id="linechart" width="600" height="330"></canvas>
            </div>
        </section>
        <!--earning graph end-->
    </div>
</div>