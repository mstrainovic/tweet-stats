package net.strainovic.tweetstats.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.auth0.NonceUtils;
import com.auth0.SessionUtils;

import net.strainovic.tweetstats.config.AppConfig;
import net.strainovic.tweetstats.util.LogHelper;

@Controller
public class LoginController {

    private static final Log LOGGER = LogFactory.getLog(LoginController.class);
    private AppConfig appConfig;

    @Autowired
    public LoginController(AppConfig appConfig) {
	this.appConfig = appConfig;
    }

    private void detectError(final Map<String, Object> model) {
	if (model.get("error") != null) {
	    model.put("error", true);
	} else {
	    model.put("error", false);
	}
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    protected String login(final Map<String, Object> model, final HttpServletRequest req) {
	LogHelper.debug(LOGGER, "Performing login");
	detectError(model);
	// add a Nonce value to session storage
	NonceUtils.addNonceToStorage(req);
	model.put("clientId", appConfig.getClientId());
	model.put("clientDomain", appConfig.getDomain());
	model.put("loginCallback", appConfig.getLoginCallback());
	model.put("state", SessionUtils.getState(req));
	return "login";
    }

}
