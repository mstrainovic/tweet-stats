package net.strainovic.tweetstats.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import net.strainovic.tweetstats.dto.CountDataDto;
import net.strainovic.tweetstats.service.SearchService;

@RestController
public class ChartController {

    private final SearchService searchService;

    @Autowired
    public ChartController(SearchService searchService) {
	this.searchService = searchService;
    }

    private SearchService getSearchService() {
	return searchService;
    }

    @ResponseBody
    @GetMapping(value = "/secured/dashboard/{hashTag}/tweetsInTime")
    public List<CountDataDto> getTweetsInTime(
	    @PathVariable(value = "hashTag", required = false) String hashTag) {
	return getSearchService().getTweetsInTimeData(hashTag);
    }

    @ResponseBody
    @GetMapping(value = "/secured/dashboard/{hashTag}/hashTagRatio")
    public List<CountDataDto> getHashTagRatio(
            @PathVariable(value = "hashTag", required = false) String hashTag) {
        return getSearchService().getHashTagRatio(hashTag);
    }

    @ResponseBody
    @GetMapping(value = "/secured/dashboard/{hashTag}/timeZoneRatio")
    public List<CountDataDto> getTimeZoneRatio(
            @PathVariable(value = "hashTag", required = false) String hashTag) {
        return getSearchService().getTimeZoneRatio(hashTag);
    }



}
