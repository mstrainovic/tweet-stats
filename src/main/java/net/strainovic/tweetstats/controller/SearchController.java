package net.strainovic.tweetstats.controller;

import java.security.Principal;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.auth0.Auth0User;

import net.strainovic.tweetstats.service.CollectingTweetService;
import net.strainovic.tweetstats.service.UserHistoryService;
import net.strainovic.tweetstats.util.HashTagHelper;
import net.strainovic.tweetstats.util.LogHelper;
import net.strainovic.tweetstats.util.UserHelper;

@Controller
public class SearchController {

    private static final String HASH = "#";

    private static final Log LOGGER = LogFactory.getLog(SearchController.class);

    private final UserHistoryService userHistoryService;

    private final CollectingTweetService collectingTweetService;

    @Autowired
    public SearchController(UserHistoryService userHistoryService, CollectingTweetService collectingTweetService) {
	this.userHistoryService = userHistoryService;
	this.collectingTweetService = collectingTweetService;
    }

    @GetMapping("/secured/search")
    protected String search() {
	LogHelper.debug(LOGGER, "Search GET request");
	return "search";
    }

    private UserHistoryService getUserHistoryService() {
	return userHistoryService;
    }

    private CollectingTweetService getCollectingTweetService() {
	return collectingTweetService;
    }

    @PostMapping(value = "/secured/search", produces = "application/json")
    protected String preformeSearch(@RequestParam String hashTag, final Principal principal) {
	final Auth0User user = (Auth0User) principal;

	if (StringUtils.isBlank(hashTag)) {
	    return "redirect:/secured/dashboard";
	}

	String userName = UserHelper.getUserName(principal);

	getUserHistoryService().addNewHistory(HashTagHelper.removeHashTag(hashTag), userName);

	// Add hashTag
	if (!hashTag.startsWith(HASH)) {
	    hashTag = HASH + hashTag;
	}

	final String searchHashTag = hashTag;
	LogHelper.debug(LOGGER, "Search for: %s, for user: %s", searchHashTag, userName);

	Runnable runnable = () -> getCollectingTweetService().searchTweetsByHashTag(searchHashTag, user);

	Thread thread = new Thread(runnable);
	thread.start();

	// getCollectingTweetService().streamTweetsByHashTag(searchHashTag,
	// user);

	// Sleep 2 second to fill some data to DB
	try {
	    Thread.sleep(2000);
	} catch (InterruptedException e) {
	    throw new RuntimeException("Failed to put thread to sleep for 2seconds", e);
	}

	return "redirect:/secured/dashboard/" + HashTagHelper.removeHashTag(hashTag);
    }

}
