package net.strainovic.tweetstats.controller;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.strainovic.tweetstats.config.AppConfig;
import net.strainovic.tweetstats.util.LogHelper;

@Controller
public class ErrorController implements org.springframework.boot.autoconfigure.web.ErrorController {

    private static final Log LOGGER = LogFactory.getLog(ErrorController.class);
    private static final String PATH = "/error";

    private AppConfig appConfig;

    @Autowired
    public ErrorController(AppConfig appConfig) {
	super();
	this.appConfig = appConfig;
    }

    @Override
    public String getErrorPath() {
	return PATH;
    }

    @RequestMapping("/error")
    protected String error(final RedirectAttributes redirectAttributes) throws IOException {
	LogHelper.error(LOGGER, "Handling error");
	final String logoutPath = appConfig.getOnLogoutRedirectTo();
	redirectAttributes.addFlashAttribute("error", true);
	return "redirect:" + logoutPath;
    }

}
