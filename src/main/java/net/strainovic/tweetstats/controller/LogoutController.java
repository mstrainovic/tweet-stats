package net.strainovic.tweetstats.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.strainovic.tweetstats.config.AppConfig;

@Controller
public class LogoutController {

    private static final Log LOGGER = LogFactory.getLog(LogoutController.class);
    private AppConfig appConfig;

    @Autowired
    public LogoutController(AppConfig appConfig) {
	this.appConfig = appConfig;
    }

    private void invalidateSession(HttpServletRequest request) {
	if (request.getSession() != null) {
	    request.getSession().invalidate();
	}
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    protected String logout(final HttpServletRequest request, final Principal principal) {
	invalidateSession(request);
	final String logoutPath = appConfig.getOnLogoutRedirectTo();
	return "redirect:" + logoutPath;
    }

}
