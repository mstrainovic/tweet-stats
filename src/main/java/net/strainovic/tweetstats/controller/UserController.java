package net.strainovic.tweetstats.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.auth0.Auth0User;
import com.auth0.jwt.internal.org.apache.commons.lang3.StringUtils;

import net.strainovic.tweetstats.dto.TweetDto;
import net.strainovic.tweetstats.dto.UserDto;
import net.strainovic.tweetstats.dto.UserHistoryDto;
import net.strainovic.tweetstats.model.UserHistory;
import net.strainovic.tweetstats.service.SearchService;
import net.strainovic.tweetstats.service.UserHistoryService;
import net.strainovic.tweetstats.util.LogHelper;
import net.strainovic.tweetstats.util.UserHelper;

@Controller
public class UserController {

    private static final Log LOGGER = LogFactory.getLog(UserController.class);
    private final SearchService searchService;
    private final UserHistoryService userHistoryService;

    @Autowired
    public UserController(SearchService searchService, UserHistoryService userHistoryService) {
	this.searchService = searchService;
	this.userHistoryService = userHistoryService;
    }

    private List<UserHistoryDto> createHistories(String userName) {

	List<UserHistory> historyForUser = getUserHistoryService().findHistoryForUser(userName);

	return historyForUser.stream()
		.map(userHistory -> new UserHistoryDto(userHistory.getHashTag(), userHistory.isProcessing()))
		.collect(Collectors.toList());
    }

    private SearchService getSearchService() {
	return searchService;
    }

    private UserHistoryService getUserHistoryService() {
	return userHistoryService;
    }

    @GetMapping("/secured/dashboard/user/{screenName}")
    protected String user(final Map<String, Object> model, final Principal principal,
	    @PathVariable("screenName") String screenName) {
	LogHelper.debug(LOGGER, String.format("Opening user page for user ID: %s", screenName));

	final Auth0User user = (Auth0User) principal;
	model.put("user", user);

	UserDto userDto = new UserDto();
	if (StringUtils.isNotBlank(screenName)) {
	    userDto = getSearchService().getUserBoardData(screenName);
	}

	String userName = UserHelper.getUserName(principal);

	List<UserHistory> userHistories = getUserHistoryService().findHistoryForUser(userName);

	Map<String, List<TweetDto>> tweetsMap = new HashMap<>();
	for (UserHistory userHistory : userHistories) {
	    String hashTag = userHistory.getHashTag();

	    List<TweetDto> tweets = getSearchService().getUserTweets(screenName, hashTag);
	    tweetsMap.put(hashTag, tweets);

	    LogHelper.debug(LOGGER, "User: %s have: %s tweets for tag: %s", screenName, tweets.size(), hashTag);
	}

	model.put("tweetsMap", tweetsMap);
	model.put("userData", userDto);
	model.put("histories", createHistories(userName));
	return "user";
    }

}
