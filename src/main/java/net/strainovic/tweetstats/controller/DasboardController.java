package net.strainovic.tweetstats.controller;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.auth0.Auth0User;

import net.strainovic.tweetstats.dto.DashboardDataDto;
import net.strainovic.tweetstats.dto.UserHistoryDto;
import net.strainovic.tweetstats.model.UserHistory;
import net.strainovic.tweetstats.service.SearchService;
import net.strainovic.tweetstats.service.UserHistoryService;
import net.strainovic.tweetstats.util.LogHelper;
import net.strainovic.tweetstats.util.UserHelper;

@Controller
public class DasboardController {

    private static final Log LOGGER = LogFactory.getLog(DasboardController.class);
    private final SearchService searchService;
    private final UserHistoryService userHistoryService;

    @Autowired
    public DasboardController(UserHistoryService userHistoryService, SearchService searchService) {
	this.userHistoryService = userHistoryService;
	this.searchService = searchService;
    }

    private List<UserHistoryDto> createHistories(String userName) {

	List<UserHistory> historyForUser = getUserHistoryService().findHistoryForUser(userName);

	return historyForUser.stream()
		.map(userHistory -> new UserHistoryDto(userHistory.getHashTag(), userHistory.isProcessing()))
		.collect(Collectors.toList());
    }

    private SearchService getSearchService() {
	return searchService;
    }

    private UserHistoryService getUserHistoryService() {
	return userHistoryService;
    }

    @RequestMapping(value = "/secured/dashboard/{hashTag}", method = RequestMethod.GET)
    protected String dashboard(final Map<String, Object> model, final Principal principal,
	    @PathVariable(value = "hashTag", required = false) String hashTag) {
	String userName = UserHelper.getUserName(principal);

	final Auth0User user = (Auth0User) principal;
	LogHelper.debug(LOGGER, "Opening dashboard page for tag: %s, for user: %s", hashTag, user.getName());
	model.put("user", user);

	LogHelper.debug(LOGGER, String.format("Opening dashboard page for tag: %s", hashTag));

	DashboardDataDto dashboardData = getSearchService().getDashboardData(hashTag);

	model.put("dashboard", dashboardData);
	model.put("histories", createHistories(userName));
	model.put("hashTag", hashTag);
	return "dashboard";
    }

    @RequestMapping(value = "/secured/dashboard/", method = RequestMethod.GET)
    protected String dashboardDefault(final Map<String, Object> model, final Principal principal) {
	String userName = UserHelper.getUserName(principal);

	final Auth0User user = (Auth0User) principal;
	LogHelper.debug(LOGGER, "Opening DEFAULT dashboard page for user: %s", user.getName());
	model.put("user", user);

	String hashTag = null;

	// Try to find from history
	List<UserHistory> histories = getUserHistoryService().findHistoryForUser(userName);

	if (CollectionUtils.isNotEmpty(histories)) {
	    histories.sort((o1, o2) -> o2.getDateCreated().compareTo(o1.getDateCreated()));
	    hashTag = histories.get(0).getHashTag();
	}

	// If there are non hashTag in history open blank dashboard
	if (StringUtils.isBlank(hashTag)) {
	    return "dashboard";
	}

	DashboardDataDto dashboardData = getSearchService().getDashboardData(hashTag);

	model.put("dashboard", dashboardData);
	model.put("histories", createHistories(userName));
	model.put("hashTag", hashTag);
	return "dashboard";
    }

}
