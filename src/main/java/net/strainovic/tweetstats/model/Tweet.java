package net.strainovic.tweetstats.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(indexes = { @Index(columnList = "hashTag") })
public class Tweet {

    private LocalDateTime createdAt;
    private int favoriteCount;
    private String hashTag;
    @ElementCollection
    private List<String> hashtagEntities;
    @Id
    private Long id;
    private boolean isFavorited;
    private boolean isPossiblySensitive;
    private boolean isRetweeted;
    private Locale language;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Media> medias;
    private int retweetCount;
    private String text;

    @ManyToOne(cascade = CascadeType.ALL)
    private TweetUser tweetUser;

    @ElementCollection
    private List<Long> userMentions;

    public LocalDateTime getCreatedAt() {
	return createdAt;
    }

    public int getFavoriteCount() {
	return favoriteCount;
    }

    public String getHashTag() {
	return hashTag;
    }

    public List<String> getHashtagEntities() {
	return hashtagEntities;
    }

    public Long getId() {
	return id;
    }

    public Locale getLanguage() {
	return language;
    }

    public List<Media> getMedias() {
	return medias;
    }

    public int getRetweetCount() {
	return retweetCount;
    }

    public String getText() {
	return text;
    }

    public TweetUser getTweetUser() {
	return tweetUser;
    }

    public List<Long> getUserMentions() {
	return userMentions;
    }

    public boolean isFavorited() {
	return isFavorited;
    }

    public boolean isPossiblySensitive() {
	return isPossiblySensitive;
    }

    public boolean isRetweeted() {
	return isRetweeted;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
	this.createdAt = createdAt;
    }

    public void setFavoriteCount(int favoriteCount) {
	this.favoriteCount = favoriteCount;
    }

    public void setFavorited(boolean favorited) {
	isFavorited = favorited;
    }

    public void setHashTag(String hashTag) {
	this.hashTag = hashTag;
    }

    public void setHashtagEntities(List<String> hashtagEntities) {
	this.hashtagEntities = hashtagEntities;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public void setLanguage(Locale language) {
	this.language = language;
    }

    public void setMedias(List<Media> medias) {
	this.medias = medias;
    }

    public void setPossiblySensitive(boolean possiblySensitive) {
	isPossiblySensitive = possiblySensitive;
    }

    public void setRetweetCount(int retweetCount) {
	this.retweetCount = retweetCount;
    }

    public void setRetweeted(boolean retweeted) {
	isRetweeted = retweeted;
    }

    public void setText(String text) {
	this.text = text;
    }

    public void setTweetUser(TweetUser tweetUser) {
	this.tweetUser = tweetUser;
    }

    public void setUserMentions(List<Long> userMentions) {
	this.userMentions = userMentions;
    }
}
