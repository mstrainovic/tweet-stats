package net.strainovic.tweetstats.model;

import org.hibernate.validator.constraints.Email;

import java.util.Date;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class TweetUser {

    @Column
    private Date createdAt;
    @Column(length = 512)
    private String description;
    @Email
    @Column
    private String email;
    @Column
    private int favouritesCount;
    @Column
    private int followersCount;
    @Column
    private int friendsCount;
    @Id
    private long id;
    @Column
    private boolean isGeoEnabled;
    @Column
    private boolean isVerified;
    @Column
    private Locale language;
    @Column
    private String location;
    @Column
    private String name;
    @Column
    private String profileBannerImageUrl;
    @Column
    private String profileImageUrl;
    @Column
    private String screenName;
    @Column
    private int statusesCount;
    @Column
    private String timeZone;
    @Column
    private int utcOffset;

    public Date getCreatedAt() {
	return createdAt;
    }

    public String getDescription() {
	return description;
    }

    public String getEmail() {
	return email;
    }

    public int getFavouritesCount() {
	return favouritesCount;
    }

    public int getFollowersCount() {
	return followersCount;
    }

    public int getFriendsCount() {
	return friendsCount;
    }

    public long getId() {
	return id;
    }

    public Locale getLanguage() {
	return language;
    }

    public String getLocation() {
	return location;
    }

    public String getName() {
	return name;
    }

    public String getProfileBannerImageUrl() {
	return profileBannerImageUrl;
    }

    public String getProfileImageUrl() {
	return profileImageUrl;
    }

    public String getScreenName() {
	return screenName;
    }

    public int getStatusesCount() {
	return statusesCount;
    }

    public String getTimeZone() {
	return timeZone;
    }

    public int getUtcOffset() {
	return utcOffset;
    }

    public boolean isGeoEnabled() {
	return isGeoEnabled;
    }

    public boolean isVerified() {
	return isVerified;
    }

    public void setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public void setFavouritesCount(int favouritesCount) {
	this.favouritesCount = favouritesCount;
    }

    public void setFollowersCount(int followersCount) {
	this.followersCount = followersCount;
    }

    public void setFriendsCount(int friendsCount) {
	this.friendsCount = friendsCount;
    }

    public void setGeoEnabled(boolean geoEnabled) {
	isGeoEnabled = geoEnabled;
    }

    public void setId(long id) {
	this.id = id;
    }

    public void setLanguage(Locale language) {
	this.language = language;
    }

    public void setLocation(String location) {
	this.location = location;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setProfileBannerImageUrl(String profileBannerImageUrl) {
	this.profileBannerImageUrl = profileBannerImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
	this.profileImageUrl = profileImageUrl;
    }

    public void setScreenName(String screenName) {
	this.screenName = screenName;
    }

    public void setStatusesCount(int statusesCount) {
	this.statusesCount = statusesCount;
    }

    public void setTimeZone(String timeZone) {
	this.timeZone = timeZone;
    }

    public void setUtcOffset(int utcOffset) {
	this.utcOffset = utcOffset;
    }

    public void setVerified(boolean verified) {
	isVerified = verified;
    }
}
