package net.strainovic.tweetstats.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Media {

    private String displayUrl;

    private Long id;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idDatabase;

    private String mediaUrl;

    public Media() {
    }

    public Media(Long id, String displayUrl, String mediaUrl) {
	this.id = id;
	this.displayUrl = displayUrl;
	this.mediaUrl = mediaUrl;
    }

    public String getDisplayUrl() {
	return displayUrl;
    }

    public Long getId() {
	return id;
    }

    public Long getIdDatabase() {
	return idDatabase;
    }

    public String getMediaUrl() {
	return mediaUrl;
    }

    public void setDisplayUrl(String displayUrl) {
	this.displayUrl = displayUrl;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public void setIdDatabase(Long idDatabase) {
	this.idDatabase = idDatabase;
    }

    public void setMediaUrl(String mediaUrl) {
	this.mediaUrl = mediaUrl;
    }
}
