package net.strainovic.tweetstats.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(indexes = { @Index(columnList = "userName") })
public class UserHistory {

    private LocalDateTime dateCreated;
    private String hashTag;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private boolean processing;
    private String userName;

    public UserHistory(LocalDateTime dateCreated, String hashTag, String userName) {
	this.dateCreated = dateCreated;
	this.hashTag = hashTag;
	this.userName = userName;
	this.processing = true;
    }

    public UserHistory() {
    }

    public LocalDateTime getDateCreated() {
	return dateCreated;
    }

    public String getHashTag() {
	return hashTag;
    }

    public Long getId() {
	return id;
    }

    public String getUserName() {
	return userName;
    }

    public boolean isProcessing() {
	return processing;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
	this.dateCreated = dateCreated;
    }

    public void setHashTag(String hashTag) {
	this.hashTag = hashTag;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public void setProcessing(boolean processing) {
	this.processing = processing;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }
}
