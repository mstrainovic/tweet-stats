package net.strainovic.tweetstats.dto;

import java.util.Date;

public class UserDto {

    private Date createdAt;
    private String description;
    private String email;
    private String favouritesCount;
    private String followersCount;
    private String friendsCount;
    private boolean verified;
    private String lang;
    private String name;
    private String profileBannerImageUrl;
    private String profileImageUrl;
    private String screenName;
    private String statusesCount;
    private String timeZone;

    public Date getCreatedAt() {
	return createdAt;
    }

    public String getDescription() {
	return description;
    }

    public String getEmail() {
	return email;
    }

    public String getLang() {
	return lang;
    }

    public String getName() {
	return name;
    }

    public String getProfileBannerImageUrl() {
	return profileBannerImageUrl;
    }

    public String getProfileImageUrl() {
	return profileImageUrl;
    }

    public String getScreenName() {
	return screenName;
    }

    public String getTimeZone() {
	return timeZone;
    }

    public boolean isVerified() {
	return verified;
    }

    public void setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public void setLang(String lang) {
	this.lang = lang;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setProfileBannerImageUrl(String profileBannerImageUrl) {
	this.profileBannerImageUrl = profileBannerImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
	this.profileImageUrl = profileImageUrl;
    }

    public void setScreenName(String screenName) {
	this.screenName = screenName;
    }

    public String getFavouritesCount() {
	return favouritesCount;
    }

    public void setFavouritesCount(String favouritesCount) {
	this.favouritesCount = favouritesCount;
    }

    public String getFollowersCount() {
	return followersCount;
    }

    public void setFollowersCount(String followersCount) {
	this.followersCount = followersCount;
    }

    public String getFriendsCount() {
	return friendsCount;
    }

    public void setFriendsCount(String friendsCount) {
	this.friendsCount = friendsCount;
    }

    public String getStatusesCount() {
	return statusesCount;
    }

    public void setStatusesCount(String statusesCount) {
	this.statusesCount = statusesCount;
    }

    public void setTimeZone(String timeZone) {
	this.timeZone = timeZone;
    }

    public void setVerified(boolean verified) {
	verified = verified;
    }

}
