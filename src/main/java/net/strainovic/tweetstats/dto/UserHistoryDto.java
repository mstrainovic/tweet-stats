package net.strainovic.tweetstats.dto;

public class UserHistoryDto {

    private String hashTag;
    private boolean processing;

    public UserHistoryDto(String hashTag) {
	this.hashTag = hashTag;
    }

    public UserHistoryDto(String hashTag, boolean processing) {
	this.hashTag = hashTag;
	this.processing = processing;
    }

    public String getHashTag() {
	return hashTag;
    }

    public boolean isProcessing() {
	return processing;
    }

    public void setHashTag(String hashTag) {
	this.hashTag = hashTag;
    }

    public void setProcessing(boolean processing) {
	this.processing = processing;
    }
}
