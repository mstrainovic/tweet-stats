package net.strainovic.tweetstats.dto;

import java.util.List;

import net.strainovic.tweetstats.model.Media;

public class TweetDto {

    private String createdAgo;
    private String favoritedCount;
    private List<Media> medias;
    private String retweetCount;
    private String text;
    private UserDto user;

    public String getCreatedAgo() {
	return createdAgo;
    }

    public String getFavoritedCount() {
	return favoritedCount;
    }

    public List<Media> getMedias() {
	return medias;
    }

    public String getRetweetCount() {
	return retweetCount;
    }

    public String getText() {
	return text;
    }

    public UserDto getUser() {
	return user;
    }

    public void setCreatedAgo(String createdAgo) {
	this.createdAgo = createdAgo;
    }

    public void setFavoritedCount(String favoritedCount) {
	this.favoritedCount = favoritedCount;
    }

    public void setMedias(List<Media> medias) {
	this.medias = medias;
    }

    public void setRetweetCount(String retweetCount) {
	this.retweetCount = retweetCount;
    }

    public void setText(String text) {
	this.text = text;
    }

    public void setUser(UserDto user) {
	this.user = user;
    }
}
