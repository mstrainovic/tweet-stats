package net.strainovic.tweetstats.dto;

public class StatisticNumbersDto {

    private String favoriteCount;
    private String retweetCount;
    private String totalTweetCount;
    private String uniqueUsers;

    public String getFavoriteCount() {
	return favoriteCount;
    }

    public void setFavoriteCount(String favoriteCount) {
	this.favoriteCount = favoriteCount;
    }

    public String getRetweetCount() {
	return retweetCount;
    }

    public void setRetweetCount(String retweetCount) {
	this.retweetCount = retweetCount;
    }

    public String getTotalTweetCount() {
	return totalTweetCount;
    }

    public void setTotalTweetCount(String totalTweetCount) {
	this.totalTweetCount = totalTweetCount;
    }

    public String getUniqueUsers() {
	return uniqueUsers;
    }

    public void setUniqueUsers(String uniqueUsers) {
	this.uniqueUsers = uniqueUsers;
    }
}
