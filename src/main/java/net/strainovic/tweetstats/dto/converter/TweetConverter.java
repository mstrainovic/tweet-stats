package net.strainovic.tweetstats.dto.converter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.ArrayUtils;

import net.strainovic.tweetstats.dto.TweetDto;
import net.strainovic.tweetstats.model.Media;
import net.strainovic.tweetstats.model.Tweet;
import twitter4j.HashtagEntity;
import twitter4j.MediaEntity;
import twitter4j.Status;
import twitter4j.UserMentionEntity;

public class TweetConverter {

    public static TweetDto fromInternalToDto(Tweet tweet) {
	TweetDto tweetDto = new TweetDto();
	tweetDto.setCreatedAgo(createCratedAgoMessage(tweet.getCreatedAt()));
	tweetDto.setUser(UserConverter.fromInternalToDto(tweet.getTweetUser()));
	tweetDto.setText(tweet.getText());
	tweetDto.setRetweetCount(NumberConverter.convertNumberToString(tweet.getRetweetCount()));
	tweetDto.setFavoritedCount(NumberConverter.convertNumberToString(tweet.getFavoriteCount()));
	tweetDto.setMedias(tweet.getMedias());
	return tweetDto;
    }

    public static List<TweetDto> fromInternalToDto(Collection<Tweet> tweets) {
	List<TweetDto> tweetDtos = new ArrayList<>();
	tweets.forEach(tweet -> tweetDtos.add(fromInternalToDto(tweet)));

	return tweetDtos;
    }

    public static Tweet fromStatusToInternal(Status status) {
	Tweet tweet = new Tweet();

	tweet.setId(status.getId());
	tweet.setCreatedAt(status.getCreatedAt().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
	tweet.setFavoriteCount(status.getFavoriteCount());

	HashtagEntity[] hashtagEntities = status.getHashtagEntities();
	List<String> hashTags = new ArrayList<>();
	if (hashtagEntities != null) {
	    for (HashtagEntity hashtag : hashtagEntities) {
		hashTags.add(hashtag.getText());
	    }
	}
	tweet.setHashtagEntities(hashTags);
	tweet.setFavorited(status.isFavorited());
	tweet.setPossiblySensitive(status.isPossiblySensitive());
	tweet.setRetweeted(status.isRetweeted());

	String lang = status.getLang();
	if (lang != null) {
	    tweet.setLanguage(Locale.forLanguageTag(lang));
	}

	tweet.setRetweetCount(status.getRetweetCount());
	tweet.setText(status.getText());

	List<Long> userMentions = new ArrayList<>();
	UserMentionEntity[] userMentionEntities = status.getUserMentionEntities();
	if (userMentionEntities != null) {
	    for (UserMentionEntity userMentionEntity : userMentionEntities) {
		userMentions.add(userMentionEntity.getId());
	    }
	}
	tweet.setUserMentions(userMentions);
	tweet.setTweetUser(UserConverter.fromTwitter4jToInternal(status.getUser()));
	tweet.setMedias(convertMedia(status.getMediaEntities()));
	return tweet;
    }

    private static List<Media> convertMedia(MediaEntity[] mediaEntities) {
	List<Media> medias = new ArrayList<>();

	if (ArrayUtils.isNotEmpty(mediaEntities)) {
	    for (MediaEntity mediaEntity : mediaEntities) {
		medias.add(new Media(mediaEntity.getId(), mediaEntity.getDisplayURL(), mediaEntity.getMediaURL()));
	    }
	}

	return medias;
    }

    private static String createCratedAgoMessage(LocalDateTime dateCreated) {
	LocalDateTime now = LocalDateTime.now();

	long diffHours = ChronoUnit.HOURS.between(dateCreated, now);
	long diffMinutes = ChronoUnit.MINUTES.between(dateCreated, now);
	long diffSeconds = ChronoUnit.SECONDS.between(dateCreated, now);

	String result;
	if (diffHours >= 1) {
	    result = diffHours + " hours ago";
	} else if (diffMinutes >= 1) {
	    result = diffMinutes + " minutes ago";
	} else {
	    result = diffSeconds + " seconds ago";
	}

	return result;
    }
}
