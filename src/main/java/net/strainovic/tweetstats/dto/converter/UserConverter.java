package net.strainovic.tweetstats.dto.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import net.strainovic.tweetstats.dto.UserDto;
import net.strainovic.tweetstats.model.TweetUser;

public class UserConverter {

    public static TweetUser fromTwitter4jToInternal(twitter4j.User dtoUser) {
	TweetUser tweetUser = new TweetUser();

	tweetUser.setId(dtoUser.getId());
	tweetUser.setCreatedAt(dtoUser.getCreatedAt());
	tweetUser.setDescription(dtoUser.getDescription());
	tweetUser.setEmail(dtoUser.getEmail());
	tweetUser.setFavouritesCount(dtoUser.getFavouritesCount());
	tweetUser.setFollowersCount(dtoUser.getFollowersCount());
	tweetUser.setFriendsCount(dtoUser.getFriendsCount());
	tweetUser.setGeoEnabled(dtoUser.isGeoEnabled());
	tweetUser.setVerified(dtoUser.isVerified());
	String langCode = dtoUser.getLang();
	if (langCode != null) {
	    tweetUser.setLanguage(Locale.forLanguageTag(langCode));
	}
	tweetUser.setLocation(dtoUser.getLocation());
	tweetUser.setName(dtoUser.getName());
	tweetUser.setProfileBannerImageUrl(dtoUser.getProfileBannerRetinaURL());
	tweetUser.setProfileImageUrl(dtoUser.getOriginalProfileImageURL());
	tweetUser.setScreenName(dtoUser.getScreenName());
	tweetUser.setStatusesCount(dtoUser.getStatusesCount());
	tweetUser.setTimeZone(dtoUser.getTimeZone());

	return tweetUser;
    }

    public static UserDto fromInternalToDto(TweetUser user) {
	UserDto userDto = new UserDto();
	userDto.setCreatedAt(user.getCreatedAt());
	userDto.setDescription(user.getDescription());
	userDto.setEmail(user.getEmail());
	userDto.setFavouritesCount(NumberConverter.convertNumberToString(user.getFavouritesCount()));
	userDto.setFollowersCount(NumberConverter.convertNumberToString(user.getFollowersCount()));
	userDto.setFriendsCount(NumberConverter.convertNumberToString(user.getFriendsCount()));
	userDto.setVerified(user.isVerified());
	userDto.setLang(user.getLanguage().getDisplayName());
	userDto.setName(user.getName());
	userDto.setProfileBannerImageUrl(user.getProfileBannerImageUrl());
	userDto.setProfileImageUrl(user.getProfileImageUrl());
	userDto.setScreenName(user.getScreenName());
	userDto.setStatusesCount(NumberConverter.convertNumberToString(user.getStatusesCount()));
	userDto.setTimeZone(user.getTimeZone());
	return userDto;
    }

    public static List<UserDto> fromInternalToDto(Collection<TweetUser> users) {
	List<UserDto> userDtos = new ArrayList<>();
	users.forEach(user -> userDtos.add(fromInternalToDto(user)));
	return userDtos;
    }
}
