package net.strainovic.tweetstats.dto;

import java.util.List;

public class DashboardDataDto {

    private StatisticNumbersDto numbers;
    private List<TweetDto> lastTweets;
    private List<TweetDto> topRetweeted;
    private List<TweetDto> topFavorited;
    private CountDataDto tweetsInTimeData;
    private List<UserDto> topFollowers;
    private List<UserDto> topStatuses;
    private List<UserDto> topFriends;

    public List<UserDto> getTopFollowers() {
        return topFollowers;
    }

    public void setTopFollowers(List<UserDto> topFollowers) {
        this.topFollowers = topFollowers;
    }

    public List<UserDto> getTopStatuses() {
        return topStatuses;
    }

    public void setTopStatuses(List<UserDto> topStatuses) {
        this.topStatuses = topStatuses;
    }

    public List<UserDto> getTopFriends() {
        return topFriends;
    }

    public void setTopFriends(List<UserDto> topFriends) {
        this.topFriends = topFriends;
    }

    public StatisticNumbersDto getNumbers() {
	return numbers;
    }

    public void setNumbers(StatisticNumbersDto numbers) {
	this.numbers = numbers;
    }

    public List<TweetDto> getLastTweets() {
	return lastTweets;
    }

    public void setLastTweets(List<TweetDto> lastTweets) {
	this.lastTweets = lastTweets;
    }

    public CountDataDto getTweetsInTimeData() {
	return tweetsInTimeData;
    }

    public void setTweetsInTimeData(CountDataDto tweetsInTimeData) {
	this.tweetsInTimeData = tweetsInTimeData;
    }

    public List<TweetDto> getTopRetweeted() {
	return topRetweeted;
    }

    public void setTopRetweeted(List<TweetDto> topRetweeted) {
	this.topRetweeted = topRetweeted;
    }

    public List<TweetDto> getTopFavorited() {
	return topFavorited;
    }

    public void setTopFavorited(List<TweetDto> topFavorited) {
	this.topFavorited = topFavorited;
    }
}
