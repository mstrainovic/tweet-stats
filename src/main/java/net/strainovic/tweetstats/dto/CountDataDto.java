package net.strainovic.tweetstats.dto;

public class CountDataDto {

    private Long count;
    private String key;

    public CountDataDto(String key, Long count) {
	this.key = key;
	this.count = count;
    }

    public Long getCount() {
	return count;
    }

    public String getKey() {
	return key;
    }

    public void setCount(Long count) {
	this.count = count;
    }

    public void setKey(String key) {
	this.key = key;
    }
}
