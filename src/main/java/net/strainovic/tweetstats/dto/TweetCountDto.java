package net.strainovic.tweetstats.dto;

import java.util.Date;

public class TweetCountDto {

    private Long _count;
    private Date _date;

    public TweetCountDto(Date date, Long count) {
	_date = date;
	_count = count;
    }

    public Long getCount() {
	return _count;
    }

    public Date getDate() {
	return _date;
    }

    public void setCount(Long count) {
	_count = count;
    }

    public void setDate(Date date) {
	_date = date;
    }

}
