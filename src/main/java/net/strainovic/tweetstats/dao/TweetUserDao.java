package net.strainovic.tweetstats.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import net.strainovic.tweetstats.model.TweetUser;

public interface TweetUserDao extends CrudRepository<TweetUser, Long> {

    @Transactional(readOnly = true)
    TweetUser findByScreenName(String screenName);
}
