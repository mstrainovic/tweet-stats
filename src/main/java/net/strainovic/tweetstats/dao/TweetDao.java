package net.strainovic.tweetstats.dao;

import java.util.List;

import net.strainovic.tweetstats.model.TweetUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import net.strainovic.tweetstats.model.Tweet;

public interface TweetDao extends CrudRepository<Tweet, Long> {

    @Transactional
    Long deleteByTweetUserAndHashTag(TweetUser tweetUser, String hashTag);

    @Transactional(readOnly = true)
    List<Tweet> findAllByHashTag(String hashTag);

    @Transactional(readOnly = true)
    List<Tweet> findAllByTweetUserAndHashTag(TweetUser tweetUser, String hashTag);
}
