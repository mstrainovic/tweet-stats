package net.strainovic.tweetstats.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import net.strainovic.tweetstats.model.UserHistory;

public interface UserHistoryDao extends CrudRepository<UserHistory, Long> {

    @Transactional
    List<UserHistory> findAllByUserNameOrderByDateCreatedDesc(String userName);

    @Transactional
    UserHistory findByUserNameAndHashTag(String userName, String hashTag);

}
