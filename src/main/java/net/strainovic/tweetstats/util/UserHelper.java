package net.strainovic.tweetstats.util;

import java.security.Principal;

import com.auth0.Auth0User;

public final class UserHelper {

    public static String getUserName(final Principal principal) {
	final Auth0User user = (Auth0User) principal;
	return (String) user.getExtraInfo().get("screen_name");
    }

    public static String getUserName(final Auth0User user) {
	return (String) user.getExtraInfo().get("screen_name");
    }

}
