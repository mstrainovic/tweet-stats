package net.strainovic.tweetstats.util;

public class HashTagHelper {

    public static String addHashTag(String search) {
	if (!search.startsWith("#")) {
	    search = "#" + search;
	}
	return search;
    }

    public static String removeHashTag(String search) {
	search = search.trim();
	if (search.startsWith("#")) {
	    search = search.substring(1);
	}
	return search;
    }
}
