package net.strainovic.tweetstats.util;

import org.apache.commons.logging.Log;

public class LogHelper {

    public static void debug(Log logger, String message) {
	if (logger.isDebugEnabled()) {
	    logger.debug(message);
	}
    }

    public static void debug(Log logger, String message, Object... args) {
	if (logger.isDebugEnabled()) {
	    logger.debug(String.format(message, args));
	}
    }

    public static void debug(Log logger, String message, Throwable e) {
	if (logger.isDebugEnabled()) {
	    logger.debug(message, e);
	}
    }

    public static void debug(Log logger, String message, Throwable e, Object... args) {
	if (logger.isDebugEnabled()) {
	    logger.debug(String.format(message, args), e);
	}
    }

    public static void error(Log logger, String message) {
	logger.error(message);
    }

    public static void error(Log logger, String message, Object... args) {
	logger.error(String.format(message, args));
    }

    public static void error(Log logger, String message, Throwable e) {
	logger.error(message, e);
    }

    public static void error(Log logger, String message, Throwable e, Object... args) {
	logger.error(String.format(message, args), e);
    }

    public static void info(Log logger, String message) {
	if (logger.isInfoEnabled()) {
	    logger.info(message);
	}
    }

    public static void info(Log logger, String message, Object... args) {
	if (logger.isInfoEnabled()) {
	    logger.info(String.format(message, args));
	}
    }

    public static void trace(Log logger, String message) {
	if (logger.isTraceEnabled()) {
	    logger.trace(message);
	}
    }

    public static void trace(Log logger, String message, Object... args) {
	if (logger.isTraceEnabled()) {
	    logger.trace(String.format(message, args));
	}
    }

    public static void warn(Log logger, String message) {
	if (logger.isWarnEnabled()) {
	    logger.warn(message);
	}
    }

    public static void warn(Log logger, String message, Object... args) {
	if (logger.isWarnEnabled()) {
	    logger.warn(String.format(message, args));
	}
    }

    public static void warn(Log logger, String message, Throwable e) {
	if (logger.isWarnEnabled()) {
	    logger.warn(message, e);
	}
    }

}