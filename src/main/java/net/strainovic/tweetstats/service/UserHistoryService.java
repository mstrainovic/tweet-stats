package net.strainovic.tweetstats.service;

import java.util.List;

import net.strainovic.tweetstats.model.UserHistory;

public interface UserHistoryService {

    void addNewHistory(String hashTag, String userName);

    UserHistory findHistoryByUserAndHashTag(String userName, String hashTag);

    List<UserHistory> findHistoryForUser(String userName);

    void saveUserHistory(UserHistory userHistory);
}
