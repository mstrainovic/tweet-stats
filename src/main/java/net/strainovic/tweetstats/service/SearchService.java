package net.strainovic.tweetstats.service;

import java.util.List;

import net.strainovic.tweetstats.dto.DashboardDataDto;
import net.strainovic.tweetstats.dto.TweetDto;
import net.strainovic.tweetstats.dto.CountDataDto;
import net.strainovic.tweetstats.dto.UserDto;

public interface SearchService {

    DashboardDataDto getDashboardData(String hashTag);

    List<CountDataDto> getHashTagRatio(String hashTag);

    List<CountDataDto> getTimeZoneRatio(String hashTag);

    UserDto getUserBoardData(String screenName);

    List<TweetDto> getUserTweets(String screenName, String hashTag);

    List<CountDataDto> getTweetsInTimeData(String hashTag);
}
