package net.strainovic.tweetstats.service;

import com.auth0.Auth0User;

public interface CollectingTweetService {

    void streamTweetsByHashTag(String hashTag, Auth0User user);

    void searchTweetsByHashTag(String hashTag, Auth0User user);
}
