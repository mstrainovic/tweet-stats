package net.strainovic.tweetstats.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.auth0.Auth0User;
import com.auth0.authentication.result.UserIdentity;

import net.strainovic.tweetstats.dao.TweetDao;
import net.strainovic.tweetstats.dto.converter.TweetConverter;
import net.strainovic.tweetstats.model.Tweet;
import net.strainovic.tweetstats.model.UserHistory;
import net.strainovic.tweetstats.service.CollectingTweetService;
import net.strainovic.tweetstats.service.UserHistoryService;
import net.strainovic.tweetstats.util.LogHelper;
import net.strainovic.tweetstats.util.UserHelper;
import twitter4j.FilterQuery;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

@Service
public class CollectingTweetServiceImpl implements CollectingTweetService {

    private static final Log LOG = LogFactory.getLog(CollectingTweetServiceImpl.class);

    private final TweetDao tweetDao;

    private final UserHistoryService userHistoryService;

    @Autowired
    public CollectingTweetServiceImpl(TweetDao tweetDao, UserHistoryService userHistoryService) {
	this.tweetDao = tweetDao;
	this.userHistoryService = userHistoryService;
    }

    @Override
    public void searchTweetsByHashTag(String hashTag, Auth0User user) {
	// Long deletedCount = getTweetDao().deleteByHashTag(hashTag);
	// LogHelper.debug(LOG, String.format("Deleted %s old tweets with %s",
	// deletedCount, hashTag));

	LogHelper.debug(LOG, String.format("Start collecting tweets with %s", hashTag));

	Twitter twitter = new TwitterFactory().getInstance();

	Query query = new Query(hashTag);
	QueryResult result;
	query.count(100);

	// Search only last 24h tweets
	query.since(LocalDate.now().minusDays(1L).toString());

	int collectedCount = 0;
	do {
	    try {
		result = twitter.search(query);
	    } catch (TwitterException e) {
		throw new RuntimeException("Error while searching data", e);
	    }

	    List<Status> statuses = result.getTweets();
	    LogHelper.debug(LOG, String.format("Founded %s tweets", statuses.size()));
	    for (Status status : statuses) {
		Tweet tweet = TweetConverter.fromStatusToInternal(status);
		tweet.setHashTag(hashTag);

		getTweetDao().save(tweet);
		collectedCount++;
	    }

	    RateLimitStatus rateLimitStatus = result.getRateLimitStatus();

	    if (rateLimitStatus.getRemaining() <= 1) {
		LogHelper.debug(LOG, String.format("Waiting for %s seconds for new request",
			rateLimitStatus.getSecondsUntilReset()));

		try {
		    Thread.sleep(rateLimitStatus.getSecondsUntilReset() * 1000);
		} catch (InterruptedException e) {
		    throw new RuntimeException("Error while waiting for API to reset timer", e);

		}
	    }
	} while ((query = result.nextQuery()) != null);

	LogHelper.debug(LOG, "All tweets with with %s collected total count: %s", hashTag, collectedCount);

	LogHelper.debug(LOG, "Stop processing history element %s", hashTag);

	String userName = UserHelper.getUserName(user);

	UserHistory userHistory = getUserHistoryService().findHistoryByUserAndHashTag(userName, hashTag);
	userHistory.setProcessing(false);
	getUserHistoryService().saveUserHistory(userHistory);
    }

    @Override
    public void streamTweetsByHashTag(String hashTag, Auth0User user) {
	LogHelper.debug(LOG, String.format("Start collecting tweets with %s", hashTag));

	UserIdentity userIdentity = user.getIdentities().get(0);
	userIdentity.getAccessToken();

	TwitterStream twitterStream = new TwitterStreamFactory().getInstance();

	MutableInt collectedCount = new MutableInt(0);

	StatusListener listener = new StatusListener() {

	    @Override
	    public void onDeletionNotice(StatusDeletionNotice arg0) {
	    }

	    @Override
	    public void onException(Exception ex) {
		ex.printStackTrace();
	    }

	    @Override
	    public void onScrubGeo(long arg0, long arg1) {
	    }

	    @Override
	    public void onStallWarning(StallWarning arg0) {
	    }

	    @Override
	    public void onStatus(Status status) {
		Tweet tweet = TweetConverter.fromStatusToInternal(status);
		tweet.setHashTag(hashTag);

		getTweetDao().save(tweet);
		collectedCount.add(1);

		if (collectedCount.getValue() % 100 == 0) {
		    LogHelper.debug(LOG, "Collected %s tweets with hash tag: %s", collectedCount.getValue(), hashTag);
		}
	    }

	    @Override
	    public void onTrackLimitationNotice(int arg0) {
	    }

	};

	twitterStream.addListener(listener);
	FilterQuery filterQuery = new FilterQuery();
	filterQuery.track(hashTag);
	twitterStream.filter(filterQuery);
    }

    private TweetDao getTweetDao() {
	return tweetDao;
    }

    private UserHistoryService getUserHistoryService() {
	return userHistoryService;
    }

}
