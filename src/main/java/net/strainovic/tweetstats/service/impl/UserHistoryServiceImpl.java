package net.strainovic.tweetstats.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.strainovic.tweetstats.dao.TweetDao;
import net.strainovic.tweetstats.dao.TweetUserDao;
import net.strainovic.tweetstats.dao.UserHistoryDao;
import net.strainovic.tweetstats.model.TweetUser;
import net.strainovic.tweetstats.model.UserHistory;
import net.strainovic.tweetstats.service.UserHistoryService;
import net.strainovic.tweetstats.util.HashTagHelper;

@Service
public class UserHistoryServiceImpl implements UserHistoryService {

    private final TweetDao tweetDao;
    private final TweetUserDao tweetUserDao;
    private final UserHistoryDao userHistoryDao;

    @Autowired
    public UserHistoryServiceImpl(UserHistoryDao userHistoryDao, TweetDao tweetDao, TweetUserDao tweetUserDao) {
	this.userHistoryDao = userHistoryDao;
	this.tweetDao = tweetDao;
	this.tweetUserDao = tweetUserDao;
    }

    @Override
    public void addNewHistory(String search, String userName) {
	List<UserHistory> userHistories = getUserHistoryDao().findAllByUserNameOrderByDateCreatedDesc(userName);

	// Save history for only last five entry
	int i = 0;
	TweetUser tweetUser = getTweetUserDao().findByScreenName(userName);

	for (UserHistory userHistory : userHistories) {
	    i++;
	    if (i >= 5) {
		getUserHistoryDao().delete(userHistory);
		getTweetDao().deleteByTweetUserAndHashTag(tweetUser, userHistory.getHashTag());
	    }
	}

	Optional<UserHistory> userHistory = userHistories.stream().filter(uh -> uh.getHashTag().equals(search))
		.findFirst();
	userHistory.ifPresent(userHistory1 -> getUserHistoryDao().delete(userHistory1));

	UserHistory newHistory = new UserHistory(LocalDateTime.now(), search, userName);
	getUserHistoryDao().save(newHistory);
    }

    @Override
    public UserHistory findHistoryByUserAndHashTag(String userName, String hashTag) {
	hashTag = HashTagHelper.removeHashTag(hashTag);
	return getUserHistoryDao().findByUserNameAndHashTag(userName, hashTag);
    }

    @Override
    public List<UserHistory> findHistoryForUser(String userName) {
	return getUserHistoryDao().findAllByUserNameOrderByDateCreatedDesc(userName);
    }

    @Override
    public void saveUserHistory(UserHistory userHistory) {
	getUserHistoryDao().save(userHistory);
    }

    private TweetDao getTweetDao() {
	return tweetDao;
    }

    private TweetUserDao getTweetUserDao() {
	return tweetUserDao;
    }

    private UserHistoryDao getUserHistoryDao() {
	return userHistoryDao;
    }
}
