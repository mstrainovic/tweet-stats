package net.strainovic.tweetstats.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.strainovic.tweetstats.dao.TweetDao;
import net.strainovic.tweetstats.dao.TweetUserDao;
import net.strainovic.tweetstats.dto.CountDataDto;
import net.strainovic.tweetstats.dto.DashboardDataDto;
import net.strainovic.tweetstats.dto.StatisticNumbersDto;
import net.strainovic.tweetstats.dto.TweetDto;
import net.strainovic.tweetstats.dto.UserDto;
import net.strainovic.tweetstats.dto.converter.NumberConverter;
import net.strainovic.tweetstats.dto.converter.TweetConverter;
import net.strainovic.tweetstats.dto.converter.UserConverter;
import net.strainovic.tweetstats.model.LimitedSortedSet;
import net.strainovic.tweetstats.model.Tweet;
import net.strainovic.tweetstats.model.TweetUser;
import net.strainovic.tweetstats.service.SearchService;
import net.strainovic.tweetstats.util.HashTagHelper;
import net.strainovic.tweetstats.util.LogHelper;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

@Service
public class SearchServiceImpl implements SearchService {

    private static final Log LOGGER = LogFactory.getLog(SearchServiceImpl.class);
    private static final String TIME_FORMAT = "%s-%s-%s %02d:%02d";
    private static final Integer TWEET_LIST_SIZE = 5;
    private static final Integer USERS_LIST_SIZE = 5;
    private final TweetDao tweetDao;

    private final TweetUserDao tweetUserDao;

    @Autowired
    public SearchServiceImpl(TweetDao tweetDao, TweetUserDao tweetUserDao) {
	this.tweetDao = tweetDao;
	this.tweetUserDao = tweetUserDao;
    }

    @Override
    public DashboardDataDto getDashboardData(String hashTag) {
	if (StringUtils.isBlank(hashTag)) {
	    LogHelper.debug(LOGGER, "HashTag is empty return default data");
	    return new DashboardDataDto();
	}

	hashTag = HashTagHelper.addHashTag(hashTag);

	LogHelper.debug(LOGGER, "START getting dashboard data for hashTag: %s", hashTag);
	long startTime = System.currentTimeMillis();

	DashboardDataDto dashboardData = new DashboardDataDto();

	List<Tweet> tweets = getTweetDao().findAllByHashTag(hashTag);

	// Tweets
	LimitedSortedSet<Tweet> topRetweeted = new LimitedSortedSet<>(TWEET_LIST_SIZE,
		(t1, t2) -> Integer.compare(t2.getRetweetCount(), t1.getRetweetCount()));

	LimitedSortedSet<Tweet> topFavorited = new LimitedSortedSet<>(TWEET_LIST_SIZE,
		(t1, t2) -> Integer.compare(t2.getFavoriteCount(), t1.getFavoriteCount()));

	LimitedSortedSet<Tweet> lastFive = new LimitedSortedSet<>(TWEET_LIST_SIZE,
		(t1, t2) -> t2.getCreatedAt().compareTo(t1.getCreatedAt()));

	// Users
	LimitedSortedSet<TweetUser> topFollowers = new LimitedSortedSet<>(USERS_LIST_SIZE,
		(u1, u2) -> Integer.compare(u2.getFollowersCount(), u1.getFollowersCount()));

	LimitedSortedSet<TweetUser> topStatuses = new LimitedSortedSet<>(USERS_LIST_SIZE,
		(u1, u2) -> Integer.compare(u2.getStatusesCount(), u1.getStatusesCount()));

	LimitedSortedSet<TweetUser> topFriends = new LimitedSortedSet<>(USERS_LIST_SIZE,
		(u1, u2) -> Integer.compare(u2.getFriendsCount(), u1.getFriendsCount()));

	// Numbers
	StatisticNumbersDto numbers = new StatisticNumbersDto();
	Set<Long> uniqueUsers = new HashSet<>();
	long favoriteCount = 0L, retweetCount = 0L;

	for (Tweet tweet : tweets) {
	    // Tweets
	    topRetweeted.add(tweet);
	    topFavorited.add(tweet);
	    lastFive.add(tweet);

	    // Users
	    topFollowers.add(tweet.getTweetUser());
	    topStatuses.add(tweet.getTweetUser());
	    topFriends.add(tweet.getTweetUser());

	    // Numbers
	    uniqueUsers.add(tweet.getTweetUser().getId());
	    favoriteCount += tweet.getFavoriteCount();
	    retweetCount += tweet.getRetweetCount();
	}

	// Tweets
	dashboardData.setTopRetweeted(TweetConverter.fromInternalToDto(topRetweeted));
	dashboardData.setTopFavorited(TweetConverter.fromInternalToDto(topFavorited));
	dashboardData.setLastTweets(TweetConverter.fromInternalToDto(lastFive));

	// Users
	dashboardData.setTopFollowers(UserConverter.fromInternalToDto(topFollowers));
	dashboardData.setTopStatuses(UserConverter.fromInternalToDto(topStatuses));
	dashboardData.setTopFriends(UserConverter.fromInternalToDto(topFriends));

	// Add numbers
	numbers.setUniqueUsers(NumberConverter.convertNumberToString(uniqueUsers.size()));
	numbers.setFavoriteCount(NumberConverter.convertNumberToString(favoriteCount));
	numbers.setRetweetCount(NumberConverter.convertNumberToString(retweetCount));
	numbers.setTotalTweetCount(NumberConverter.convertNumberToString(tweets.size()));
	dashboardData.setNumbers(numbers);

	long endTime = System.currentTimeMillis();
	LogHelper.debug(LOGGER, "END getting dashboard data for hashTag: %s for total of %s milliseconds", hashTag,
		(endTime - startTime));
	return dashboardData;
    }

    @Override
    public List<CountDataDto> getHashTagRatio(String hashTag) {
	List<CountDataDto> countDataDtos = new ArrayList<>();

	List<Tweet> tweets = getTweetDao().findAllByHashTag(hashTag);

	tweets.stream().flatMap(t -> t.getHashtagEntities().stream())
		.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
		.forEach((key, value) -> countDataDtos.add(new CountDataDto(key, value)));

	return countDataDtos;
    }

    @Override
    public List<CountDataDto> getTimeZoneRatio(String hashTag) {
	List<CountDataDto> countDataDtos = new ArrayList<>();

	List<Tweet> tweets = getTweetDao().findAllByHashTag(hashTag);

	tweets.stream().map(t -> t.getTweetUser().getTimeZone())
		.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
		.forEach((key, value) -> countDataDtos.add(new CountDataDto(key, value)));

	return countDataDtos;
    }

    public List<CountDataDto> getTweetsInTimeData(String hashTag) {
	hashTag = HashTagHelper.addHashTag(hashTag);

	List<Tweet> tweets = getTweetDao().findAllByHashTag(hashTag);

	List<CountDataDto> countDataDtos = new ArrayList<>();

	if (CollectionUtils.isNotEmpty(tweets)) {
	    LogHelper.debug(LOGGER, "There are %s tweets for hashTag: %s", tweets.size(), hashTag);

	    LocalDateTime firstDate = tweets.get(0).getCreatedAt();
	    LocalDateTime lastDate = tweets.get(tweets.size() - 1).getCreatedAt();

	    long hoursBetween = ChronoUnit.HOURS.between(firstDate, lastDate);
	    long minutesBetween = ChronoUnit.MINUTES.between(firstDate, lastDate);

	    List<String> result = new ArrayList<>();

	    int minutes = 2;
	    if (hoursBetween > 0) {
		minutes = (int) (hoursBetween * 10);
	    } else if (minutesBetween > 30) {
		minutes = 5;
	    }

	    for (Tweet tweet : tweets) {
		LocalDateTime dateTime = tweet.getCreatedAt();
		dateTime = dateTime.plusMinutes((60 + minutes - dateTime.getMinute()) % minutes);
		LocalDate localDate = dateTime.toLocalDate();
		String formatted = String.format(TIME_FORMAT, localDate.getYear(), localDate.getMonthValue(),
			localDate.getDayOfMonth(), dateTime.getHour(), dateTime.getMinute());
		result.add(formatted);
	    }

	    result.stream().collect(Collectors.groupingBy(t -> t, Collectors.counting())).entrySet().stream()
		    .sorted(Map.Entry.comparingByKey())
		    .forEachOrdered(x -> countDataDtos.add(new CountDataDto(x.getKey(), x.getValue())));

	} else {
	    LogHelper.debug(LOGGER, "There are no tweets for hashTag: %s", hashTag);
	}
	return countDataDtos;
    }

    @Override
    public UserDto getUserBoardData(String screenName) {
	Twitter twitter = new TwitterFactory().getInstance();

	User user;
	try {
	    user = twitter.showUser(screenName);
	} catch (TwitterException e) {
	    throw new RuntimeException(String.format("Error getting user with screenName: %s from twitter", screenName),
		    e);
	}

	TweetUser tweetUser = UserConverter.fromTwitter4jToInternal(user);

	return UserConverter.fromInternalToDto(tweetUser);
    }

    @Override
    public List<TweetDto> getUserTweets(String screenName, String hashTag) {
	TweetUser tweetUser = getTweetUserDao().findByScreenName(screenName);
	List<Tweet> tweets = getTweetDao().findAllByTweetUserAndHashTag(tweetUser, HashTagHelper.addHashTag(hashTag));

	return TweetConverter.fromInternalToDto(tweets);
    }

    private TweetDao getTweetDao() {
	return tweetDao;
    }

    private TweetUserDao getTweetUserDao() {
	return tweetUserDao;
    }
}
