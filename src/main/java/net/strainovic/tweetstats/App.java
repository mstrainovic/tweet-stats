package net.strainovic.tweetstats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@ComponentScan(basePackages = { "net.strainovic.tweetstats", "com.auth0.web" })
@EnableAutoConfiguration
@PropertySources({ @PropertySource("classpath:application.yml"), @PropertySource("classpath:auth0.properties"),
	@PropertySource("classpath:twitter4j.properties") })
public class App {

    public static void main(final String[] args) throws Exception {
	SpringApplication.run(App.class, args);
    }

}
